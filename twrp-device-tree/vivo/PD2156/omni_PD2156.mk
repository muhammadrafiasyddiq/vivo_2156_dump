#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from PD2156 device
$(call inherit-product, device/vivo/PD2156/device.mk)

PRODUCT_DEVICE := PD2156
PRODUCT_NAME := omni_PD2156
PRODUCT_BRAND := vivo
PRODUCT_MODEL := V2156A
PRODUCT_MANUFACTURER := vivo

PRODUCT_GMS_CLIENTID_BASE := android-vivo

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="full_k6833v1_64-user 11 RP1A.200720.012 eng.compil.20230203.201450 release-keys"

BUILD_FINGERPRINT := vivo/PD2156/PD2156:11/RP1A.200720.012/compiler0203201230:user/release-keys
