#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Include GSI keys
$(call inherit-product, $(SRC_TARGET_DIR)/product/gsi_keys.mk)

# fastbootd
PRODUCT_PACKAGES += \
    android.hardware.fastboot@1.1-impl-mock \
    fastbootd

# Health
PRODUCT_PACKAGES += \
    android.hardware.health@2.1-impl \
    android.hardware.health@2.1-impl.recovery \
    android.hardware.health@2.1-service

# Overlays
PRODUCT_ENFORCE_RRO_TARGETS := *

# Partitions
PRODUCT_BUILD_SUPER_PARTITION := false
PRODUCT_USE_DYNAMIC_PARTITIONS := true

# Product characteristics
PRODUCT_CHARACTERISTICS := default

# Rootdir
PRODUCT_PACKAGES += \
    init.vivo.fingerprint_restart_counter.sh \
    install-recovery.sh \
    collect_connsys_dump.sh \
    zramsize_reconfig.sh \
    init.vivo.fingerprint.sh \
    init.vivo.crashdata.sh \

PRODUCT_PACKAGES += \
    fstab.mt6833 \
    factory_init.project.rc \
    init.modem.rc \
    init.mt6833.usb.rc \
    meta_init.project.rc \
    init.mt6833.rc \
    init.connectivity.rc \
    init.ago.rc \
    init.aee.rc \
    factory_init.rc \
    init.sensor_2_0.rc \
    factory_init.connectivity.rc \
    meta_init.connectivity.rc \
    init.project.rc \
    multi_init.rc \
    meta_init.modem.rc \
    init.factory.rc \
    meta_init.rc \
    init.recovery.wifi.rc \
    init.recovery.svc.rc \
    init.recovery.platform.rc \
    init.recovery.touch.rc \
    init.recovery.mt6833.rc \

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/rootdir/etc/fstab.mt6833:$(TARGET_COPY_OUT_RAMDISK)/fstab.mt6833

# Shipping API level
PRODUCT_SHIPPING_API_LEVEL := 30

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

# Inherit the proprietary files
$(call inherit-product, vendor/vivo/PD2156/PD2156-vendor.mk)
