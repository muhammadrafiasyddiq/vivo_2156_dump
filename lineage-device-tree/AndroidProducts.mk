#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_PD2156.mk

COMMON_LUNCH_CHOICES := \
    lineage_PD2156-user \
    lineage_PD2156-userdebug \
    lineage_PD2156-eng
